package Fibo;
import java.lang.reflect.Array;
import java.util.Scanner;

import Fibo.Main.Pair;

public class Main {
	public static class Pair<E> {
		private E first;
		private E second;
		public E getFirst() { return first;}
		public void first(E first) { this.first = first;}
		public E second() { return second;}
		public Pair(E first, E second) { super(); this.first = first; this.second = second; }
	}
	
	
	public static void main(String[] args){
		Scanner in = new Scanner(System.in);
		
		System.out.println(" ");
		
		long num = in.nextLong();
		
		Pair<Long> result = fibonacci(num);
		
		System.out.println(fibonacci(num).first);
		

		
	}
	
	public static  Pair<Long> fibonacci(long n){
		if(n == 0 || n ==1)
	        return new Pair<Long>(1L, 1L);
	   
	   else{
		  Pair<Long> res = fibonacci(n-1);
		  
		  return new Pair<Long>(res.first+res.second, res.first);
	   }
	      
	}

}
