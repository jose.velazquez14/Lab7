package PowerOfTwo;
import java.util.Scanner;

public class Main {
	
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		
	
		long number = in.nextLong();
		
		if(isPowerOfTwo(number)){
			System.out.println("yes");
		}
		else
			System.out.println("no");
			
	}
	
	public static boolean isPowerOfTwo(long n){
		if(n==1)
			return true;
		if(n%2==0) return isPowerOfTwo(n/2);
		
		return false;
	}
	
}
